<?php

namespace Tests;

//use Mockery as m;
use PHPUnit\Framework\TestCase;
use App\Mobile;
use App\ClaroProvider;

class MobileTest extends TestCase
{
	
	/** @test */
	public function it_returns_null_when_name_empty()
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertNull($mobile->makeCallByName(''));
	}

	/** @test */
	public function returns_value_when_name_exists()
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertEquals(
			'jose',
			$mobile->makeCallByName('jose')
		);
	}

	/** @test */
	public function returns_empty_when_name_does_not_exist()
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertNull(			
			$mobile->makeCallByName('mauro')
		);
	}
	
	/** @test */
	public function returns_1_when_the_message_was_sent()
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertEquals(
			1,
			$mobile->sendMessage('980549451', 'es un mensaje del usuario')
		);
	}
	
	/** @test */
	public function returns_2_when_the_number_does_not_exist()	
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertEquals(
			2,
			$mobile->sendMessage('999999999', 'es un mensaje del usuario')
		);
	}

	/** @test */
	public function returns_3_when_the_number_is_empty()	
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertEquals(
			3,
			$mobile->sendMessage('', 'es un mensaje del usuario')
		);
	}

	/** @test */
	public function returns_4_when_the_message_is_empty()	
	{
		$provider = new ClaroProvider;

		$mobile = new Mobile($provider);

		$this->assertEquals(
			4,
			$mobile->sendMessage('980549451', '')
		);
	}

	
}
