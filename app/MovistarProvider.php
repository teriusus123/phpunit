<?php 
namespace App;

use App\Interfaces\CarrierInterface;

class MovistarProvider implements CarrierInterface
{
     private $contact    = null;
     private $number     = null;
     private $message    = null;     

     public function dialContact($contact)
     {
          $this->contact = $contact;
     }

     public function makeCall()
     {    
          return $this->contact;
     }

     public function dialNumber($number)
     {
          $this->number = $number;
     }

     public function makeCallByNumber()
     {
          return $this->number;
     }
     
     public function writeMessage($message)
     {
          $this->message = $message;
     }

     public function sendMessage()
     {
          return $this->message;
     }
     
}


