<?php

namespace App;

use App\Services\ContactService;
class Mobile
{

	protected $provider;

	function __construct($provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName($name = '')
	{
		if( empty($name) ) return;

		$contact = ContactService::findByName($name);

		$this->provider->dialContact($contact);

		return $this->provider->makeCall();
	}

	public function sendMessage($cell = '', $message = '')
	{
		if( empty($cell) ) return 3;
		if( empty($message) ) return 4;

		$number 	= ContactService::validateNumber($cell);
		$msg		= ContactService::validateMessage($message);
		
		if( empty($number) ) return 2;		

		$this->provider->dialNumber($number);
		$this->provider->writeMessage($message);

		$gNumber = $this->provider->makeCallByNumber();
		$gMessage = $this->provider->sendMessage();		

		return ($gNumber && $gMessage)? true : false;
	}

}
