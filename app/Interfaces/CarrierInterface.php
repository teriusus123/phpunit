<?php

namespace App\Interfaces;
interface CarrierInterface
{
	public function dialContact(string $contact);

	public function makeCall();

	public function dialNumber(string $number);
	
	public function makeCallByNumber();

	public function writeMessage(string $message);

	public function sendMessage();
}
