<?php

namespace App\Services;

use App\Contact;
use App\Call;
class ContactService
{	
	public static function findByName($name)
	{
		$contact = new Contact();

		return self::findValue($contact, $name);
	}

	public static function validateNumber(string $number)
	{
		if( empty($number) ) return;
		if( !is_numeric($number) ) return;		

		$call = new Call();

		return self::findValue($call, $number);
	}

	private static function findValue($obj, $name)
	{
		$resp	= null;		
		$records 	= $obj->records();

		if(in_array($name, $records)){
			$index = array_search($name, $records);
			$resp = is_numeric($index)? $records[$index] : null;
		}

		return $resp;
	}

	public static function validateMessage(string $message)
	{
		return empty($message)? false : true;
	}
}